#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
    if (argc > 1) {
        int i {1};
        while (i < argc) {
            cout << "Processing " << argv[i] << "\n";
            vector<string> vrec;
            ifstream ifs(argv[i], ifstream::in);
            if (!ifs.is_open()) {
                cout << "Cannot open " << argv[i] << "\n";
                ++i;
            } else {
                string s;
                bool continuation {false};
                while ( getline(ifs, s, '\'')) {
                    bool concat {false};
                    if (continuation == true) {
                        vrec.back() += s;
                        vrec.back() += '\'';
                        if (s.back() != '?') // dont unset continuation if multiple ?'
                            continuation = false;
                        concat = true;
                    }
                    if (s.back() == '?') {
                        continuation = true;
                    }
                    if (!concat) {
                        s += '\'';
                        vrec.push_back(s);
                    }
                }
                for (auto it = begin(vrec); it != end(vrec); ++it)
                    cout << *it << "\n";

                ++i;
            }

        }
    }
}
